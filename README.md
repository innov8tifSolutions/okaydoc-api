
## OkayDoc - Document Verification MyKad
## Version 3

```yaml

URL: http://okaydocdemo.innov8tif.com/ekyc/api/ekyc/v3/doc-verify/mykad
Method: POST
Request: 
   Mandatory Parameter:
     
        *  apiKey: Request from Innov8tif (ekycsupport@innov8tif.com)
        *  idImageBase64Image: MyKad Image 
        *  idNumber: ID No 
        *  otherDocList:
            *  with type "high_quality" - High Resolution image - 8MP and above. Only applicable for microprint analysis.  

   Optional Parameter:
        
        * otherDocList type can be: high_quality, with_flash, without_flash without any specific order.
        * landmarkCheck: default as False
        * fontCheck: default as False
        * microprintCheck: default as False
        * photoSubstitutionCheck: default as False
        * icTypeCheck: default as False
        * colorMode: default as False

```

## Recommmeded Threhsold
<table>
<tbody>
<tr>
<td><strong>Item</strong></td>
<td><strong>Threshold</strong></td>
</tr>
<tr>
<td>
<div>
<div>MyKad Header(Kad Pengenalan)</div>
</div>
</td>
<td>30.0</td>
</tr>
<tr>
<td>
<div>
<div>MyKad Logo</div>
</div>
</td>
<td>30.0</td>
</tr>
<tr>
<td>
<div>
<div>Malaysia Flag</div>
</div>
</td>
<td>30.0</td>
</tr>
<tr>
<td>
<div>
<div>Chip</div>
</div>
</td>
<td>30.0</td>
</tr>
<tr>
<td>
<div>
<div>Hibiscus</div>
</div>
</td>
<td>30.0</td>
</tr>
<tr>
<td>MSC</td>
<td>30.0</td>
</tr>
<tr>
<td>Font&nbsp;</td>
<td>
<ul>
<li>60.0</li>
<li>NULL - If font is not detected from the image</li>
</ul>
</td>
</tr>
<tr>
<td>
<div>
<div>Microprint Score</div>
</div>
</td>
<td>45, Please refer to&nbsp;<a title="Here" href="https://innov8tif.com/wp-content/uploads/2018/03/emas-ekyc-okaydoc-microprint.jpg" target="_blank" rel="noopener">Here</a></td>
</tr>
<tr>
<td>
<div>
<div>Substitution Score</div>
</div>
</td>
<td>65.3</td>
</tr>
<tr>
<td>
<div>
<div>IC Type</div>
</div>
</td>
<td>
<ul>
<li>New IC - Please refer to <a href="https://www.jpn.gov.my/wp-content/uploads/mykad_2-370x1024.jpg" target="_blank" rel="noopener">Here</a></li>
<li>Old IC - Please refer to&nbsp;<a title="Here" href="https://www.thestar.com.my/~/media/online/2015/05/07/21/20/main_0805_p31_sf_1pdf.ashx/?w=620&amp;h=413&amp;crop=1&amp;hash=DBA17445EE99A2E130F26E020C5AE1E410BDD264" target="_blank" rel="noopener">Here</a></li>
</ul>
</td>
</tr>
<tr>
<td>
<div>
<div>Color</div>
</div>
</td>
<td>
<ul>
<li>Color - Color Image&nbsp;</li>
<li>Black &amp; White - Photocopy&nbsp;</li>
</ul>
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>



## SAMPLE - API Request (JSON Object)
```yaml
{
    "apiKey": "T1XmeCOWeSfAWDUDobYbJx3i2CMEhCXF",
    "idImageBase64Image": "/9j/4AAQSkZJ...fYs1wRtQHt//Z\r\n",
    "idNumber": "820403-05-1111",
    "name": "LIM KEN WEI",
    "address": "address",
    "faceidScore": null,
    "livenessFaceBase64Image": "/9j/4AAQSkZJ...fYs1wRtQHt//Z\r\n",
    "frontCameraLux": null,
    "backCameraLux": null,
    "phoneInformation": {
        "latitude": 100,
        "longitude": 200,
        "iMEINumber": "iMEINumber",
        "phoneNumber": "phoneNumber",
        "phoneModel": "phoneModel",
        "osVersion": "osVersion",
        "local": "local",
        "ipAddress": "ipAddress" 
    },
    "otherDocList": [{
        "base64Image": "/9j/4AAQSkZ...p41fYs1wRtQHt//Z\r\n",
        "type": "high_quality" 
    },
    {
        "base64Image": "/9j/4AAQSk...Ys1wRtQHt//Z\r\n",
        "type": "with_flash" 
    },
    {
        "base64Image": "/9j/4AAQSkZJ...Ys1wRtQHt//Z\r\n",
        "type": "without_flash" 
    },
    {
        "base64Image": "/9j/4AAQSk...s1wRtQHt//Z\r\n",
        "type": "without_flash" 
    }],
    "landmarkCheck": true,
    "fontCheck": true,
    "microprintCheck": true,
    "photoSubstitutionCheck": true,
    "icTypeCheck": true,
    "colorMode": true
}



```


## SAMPLE - Response (JSON Object)
```yaml

{
    "status": "success",
    "messageCode": "api.success",
    "message": "",
    "methodList": [{
        "method": "landmark",
        "label": "Landmark Analysis Result",
        "componentList": [{
            "code": "l-mykad-header",
            "label": "MyKad Header(Kad Pengenalan)",
            "value": "0.79320114851",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=l-mykad-header",
            "refImageUrl": null
        },
        {
            "code": "l-mykad-logo",
            "label": "MyKad Logo",
            "value": "0.727208554745",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=l-mykad-logo",
            "refImageUrl": null
        },
        {
            "code": "l-my-flag-logo",
            "label": "Malaysia Flag",
            "value": "0.71590679884",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=l-my-flag-logo",
            "refImageUrl": null
        },
        {
            "code": "l-chip",
            "label": "Chip",
            "value": "0.784151494503",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=l-chip",
            "refImageUrl": null
        },
        {
            "code": "l-hibiscus",
            "label": "Hibiscus",
            "value": "0.771929860115",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=l-hibiscus",
            "refImageUrl": null
        },
        {
            "code": "l-msc",
            "label": "MSC",
            "value": "0.899240195751",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=l-msc",
            "refImageUrl": null
        }]
    },
    {
        "method": "belowImageText",
        "label": "Text Below IC Face",
        "componentList": [{
            "code": "belowImageText",
            "label": "Text Below IC Face",
            "value": "WAROANeOARA",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=belowImageText",
            "refImageUrl": null
        }]
    },
    {
        "method": "font",
        "label": "MyKad Front Checking Result",
        "componentList": [{
            "code": "f-id-no",
            "label": "ID No",
            "value": "0.706493735313",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=f-id-no",
            "refImageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=f-id-no-ref" 
        }]
    },
    {
        "method": "microprint",
        "label": "Microprint Score",
        "componentList": [{
            "code": "microprint",
            "label": "Microprint",
            "value": "1",
            "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=microprint",
            "refImageUrl": null
        }]
    },
    {
        "method": "substitution",
        "label": "Substitution Checking",
        "componentList": [{
            "code": "substitution",
            "label": "Substitution Score",
            "value": "79.86243",
            "imageUrl": null,
            "refImageUrl": null
        }]
    },
    {
        "method": "docType",
        "label": "MyKad Type",
        "componentList": [{
            "code": "docType",
            "label": "IC Type",
            "value": "New IC",
            "imageUrl": null,
            "refImageUrl": null
        }]
    },
    {
        "method": "colorMode",
        "label": "Color Mode",
        "componentList": [{
            "code": "colorMode",
            "label": "Color",
            "value": "Colored Image",
            "imageUrl": null,
            "refImageUrl": null
        }]
    }],
    "outputImageList": [{
        "tag": "holoFace",
        "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=holoFace" 
    },
    {
        "tag": "idFace",
        "imageUrl": "/public/image/v2/d569a1a1-9ed2-42ea-ba8a-3f59ccd26db6?type=idFace" 
    }]
}


```

